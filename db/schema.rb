# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171118234931) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "adminpack"

  create_table "osobas", force: :cascade do |t|
    t.string   "jmeno",      limit: 255
    t.string   "prijmeni",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "prace_polozkas", force: :cascade do |t|
    t.integer  "osoba_id"
    t.integer  "projekt_id"
    t.date     "datum"
    t.text     "obsah"
    t.float    "hodiny"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "projekts", force: :cascade do |t|
    t.string   "nazev",      limit: 255
    t.integer  "kod"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",                 default: true
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",           limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest", limit: 255
    t.integer  "osoba_id"
    t.string   "remember_token",  limit: 255
    t.boolean  "admin"
    t.boolean  "active",                      default: true
  end

  add_index "users", ["remember_token"], name: "index_users_on_remember_token", using: :btree

end
