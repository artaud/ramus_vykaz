class AddActiveToProjekts < ActiveRecord::Migration
  def change
    add_column :projekts, :active, :boolean, :default => true
  end
end
