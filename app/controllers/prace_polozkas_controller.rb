class PracePolozkasController < ApplicationController
  before_action :set_prace_polozka, only: [:show, :edit, :update, :destroy]
  # GET /prace_polozkas
  # GET /prace_polozkas.json
  def index
    @prace_polozkas = PracePolozka.all
    if signed_in?
      @aktualni_hodiny = PracePolozka.where(osoba_id: current_user.osoba_id).sum('hodiny')
    end
  end

  def settings
    @osobas = Osoba.all
    @projekts = Projekt.all
    @users = User.all

    if request.post?

      direction = params['direction']

      if params.key?('user_id')
        userid = params['user_id']
        u = User.find_by(id: userid)
        u.update_attribute(:active, direction)
      end

      if params.key?('project_id')
        projectid = params['project_id']
        pr = Projekt.find_by(id: projectid)
        pr.update_attribute(:active, direction)
      end

    end
  end

  def statistika

    def get_projekt_nazev(projekt_id)
      return "banana"
    end

    @prace_polozkas = PracePolozka.all
    @projekts = Projekt.all
    @osobas = Osoba.all

    @mesicni_hodiny = []
    @osobas.each do |os|
      if User.find_by(id: os.id).active
        @mesicni_hodiny << [os.id, os.prijmeni, os, os.prace_polozkas.group("date_trunc('month', datum)").sum('hodiny')]
      end
    end

    @mesicni_hodiny = @mesicni_hodiny.sort_by { |_a, b| b }
    @projektove_hodiny = []
    @projekts.each do |pr|
      @projektove_hodiny << [pr.id, pr, pr.prace_polozkas.group("date_trunc('month',datum)").sum('hodiny')]
    end

    @months_worked = {}
    @months_worked_osobas = {}
    months_worked = @prace_polozkas.map { |pp| [pp.datum.month, pp.datum.year].join(' ') }.uniq
    # byebug
    months_worked.each do |month|
      res = @prace_polozkas.select do |pp|
        [pp[:datum].month, pp[:datum].year].join(' ') == month
      end
      sum_by_project = res.group_by(&:projekt_id).map { |x, y| [x, y.inject(0) { |sum, i| sum + i.hodiny }] }
      sum_by_project = sum_by_project.sort_by { |a| a[0] }

      # res jsou pracepolozky v tomto mesici
      # chci je sgrupovat podle lidi, ale pak je rozdelit.
      # krok 1 sgrupovat podle lidi

      # sum_by_osoba vraci:
      # [[8, 14.0, [[3, 2.0], [8, 10.5], [5, 0.5], [20, 1.0]]], [5, 20.5, [[5, 5.0], [8, 9.0], [2, 6.5]]], .... ]
      #  tj. pro dany mesic:
      # [[osoba_id, hodiny za mesic soucet, [[projekt_id, hodiny], [projekt_id, hodiny], ...]], [osoba_id, hodiny za mesic soucet, [[projekt_id, hodiny], [projekt_id, hodiny], ...]], .... ]


      sum_by_osoba = res.group_by(&:osoba_id).map { |x, y| [x, y.inject(0) { |sum, i| sum + i.hodiny },y.group_by(&:projekt_id).map { |proj, hod| [proj, hod.inject(0) { |souc, et| souc + et.hodiny} ]}.sort_by { |a| a[0]} ] }
      sum_by_osoba = sum_by_osoba.sort_by { |a| a[0] }

      # byebug

      @months_worked[month] = sum_by_project
      @months_worked_osobas[month] = sum_by_osoba
    end

    # byebug

    # projekty_po_mesici_dle_cloveka = Osoba.all.product([1]).to_h
    # projekty_po_mesici_dle_cloveka.each do |os|
    # mesicni_projektove_hodiny = Osoba.all.product([1]).to_h

    @mesicni_projektove_hodiny = {}
    @osobas.each do |os|
      @mesicni_projektove_hodiny[os.id] = [os.prace_polozkas]
    end
  end

  def month_statistika
    @prace_polozkas = PracePolozka.all
    @projekts = Projekt.all
    @osobas = Osoba.all
    @mesicni_hodiny = []
    @osobas.each do |os|
      @mesicni_hodiny << [os.id, os, os.prace_polozkas.group("date_trunc('month', datum)").sum('hodiny')]
    end
    # @range = (1.year.ago.to_date..DateTime.now.to_date).to_s
    # @unfinished_range = (1.year.ago.to_date..DateTime.now.to_date).group_by(&:month).map { |_,v| (v.first.month.to_s + "/" + v.first.year.to_s) }
  end

  # GET /prace_polozkas/1
  # GET /prace_polozkas/1.json
  def show
  end

  # GET /prace_polozkas/new
  def new
    @prace_polozka = PracePolozka.new
    projektsort
    # projektchart
    @active_projekts = Projekt.where(active: true).collect {|p| [p.nazev, p.id]}.sort
  end

  # GET /prace_polozkas/1/edit
  def edit
    projektsort
    # projektchart
    @active_projekts = Projekt.where(active: true).collect {|p| [p.nazev, p.id]}.sort
    @osobas = Osoba.all.collect {|p| [p.jmeno.to_s + " " + p.prijmeni.to_s, p.id]}.sort
  end

  # POST /prace_polozkas
  # POST /prace_polozkas.json
  def create
    @prace_polozka = PracePolozka.new(prace_polozka_params)

    respond_to do |format|
      if @prace_polozka.save
        format.html { redirect_to action: 'index' } # changed to render index from redirect_to @prace_polozka
        format.json { render action: 'show', status: :created, location: @prace_polozka }
        flash[:notice] = "Záznam byl vytvořen!"
      else
        format.html { render action: 'new' }
        format.json { render json: @prace_polozka.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /prace_polozkas/1
  # PATCH/PUT /prace_polozkas/1.json
  def update
    respond_to do |format|
      if @prace_polozka.update(prace_polozka_params)
        format.html { redirect_to action: 'index', notice: 'Zaznam byl zmenen.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @prace_polozka.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /prace_polozkas/1
  # DELETE /prace_polozkas/1.json
  def destroy
    @prace_polozka.destroy
    respond_to do |format|
      format.html { redirect_to prace_polozkas_url }
      format.json { head :no_content }
    end
  end


  private

  # Use callbacks to share common setup or constraints between actions.
  def set_prace_polozka
    @prace_polozka = PracePolozka.find(params[:id])
  end

  def projektsort
    @projekts = Projekt.all.collect {|p| [p.nazev, p.id]}.sort
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def prace_polozka_params
    params.require(:prace_polozka).permit(:osoba_id, :projekt_id, :datum, :obsah, :hodiny)
  end

  def projektchart
        # Org chart
        data_table1 = GoogleVisualr::DataTable.new
        data_table2 = GoogleVisualr::DataTable.new
        data_table3 = GoogleVisualr::DataTable.new
        data_table4 = GoogleVisualr::DataTable.new
    # Add Column Headers
    data_table1.new_column('string', 'Nazev a kod')
    data_table1.new_column('string', 'Rodic')
    data_table1.new_column('string', 'Tooltip')
    data_table2.new_column('string', 'Nazev a kod')
    data_table2.new_column('string', 'Rodic')
    data_table2.new_column('string', 'Tooltip')
    data_table3.new_column('string', 'Nazev a kod')
    data_table3.new_column('string', 'Rodic')
    data_table3.new_column('string', 'Tooltip')
    data_table4.new_column('string', 'Nazev a kod')
    data_table4.new_column('string', 'Rodic')
    data_table4.new_column('string', 'Tooltip')
    # Add Rows and Values
    datarows = Array.new
      # Sort projects into tree structure
      Projekt.all.each do |proj|

        onerow = Array.new(3)
        onerow[0] = proj.kod.to_s + ' ' + proj.nazev  #actual node TODO add html graphics

        puts proj.inspect

        if proj.kod.to_s.length == 1
          parent = ''
          puts 'length 1, nazev: ' + proj.nazev + ', parent: ' + parent
        end

        if proj.kod.to_s.length == 3
          tkod = proj.kod.to_s[0,1]
          parent_proj = Projekt.find_by kod: tkod.to_i
          puts 'length 3, nazev: ' + proj.nazev + ', parent proj: ' + parent_proj.inspect
          parent = parent_proj.kod.to_s + ' ' + parent_proj.nazev
        end

        if proj.kod.to_s.length == 4
          tkod = proj.kod.to_s[0,3]
          parent_proj = Projekt.find_by kod: tkod.to_i
          puts 'length 4, nazev: ' + proj.nazev + ', parent proj: ' + parent_proj.inspect
          parent = parent_proj.kod.to_s + ' ' + parent_proj.nazev
        end

          onerow[1] = parent      #parent node
          onerow[2] = proj.nazev  #tooltip
          datarows << onerow
        end

        datarows = datarows.sort

        datarows1 = datarows.select {|row| row[0][0] == '1'}
        datarows2 = datarows.select {|row| row[0][0] == '2'}
        datarows3 = datarows.select {|row| row[0][0] == '3'}
        datarows4 = datarows.select {|row| row[0][0] == '4'}
        data_table1.add_rows(datarows1)
        data_table2.add_rows(datarows2)
        data_table3.add_rows(datarows3)
        data_table4.add_rows(datarows4)

        # puts datarows
        # puts datarows1
        # puts datarows2


    # asdd

    option = { size: 'medium', :allowHtml => true, allowCollapse: true }
    @chart1 = GoogleVisualr::Interactive::OrgChart.new(data_table1, option)
    @chart2 = GoogleVisualr::Interactive::OrgChart.new(data_table2, option)
    @chart3 = GoogleVisualr::Interactive::OrgChart.new(data_table3, option)
    @chart4 = GoogleVisualr::Interactive::OrgChart.new(data_table4, option)
  end
end
