class ProjektsController < ApplicationController
  # GET /projekts/new
  def new
    @projekt = Projekt.new
  end

  def create
    @projekt = Projekt.new(projekt_params)

    respond_to do |format|
      if @projekt.save
        format.html { redirect_to action: 'new' } # changed to render index from redirect_to @projekt
        format.json { render action: 'show', status: :created, location: @projekt }
        flash[:notice] = "Záznam byl vytvořen!"
      else
        format.html { render action: 'new' }
        format.json { render json: @projekt.errors, status: :unprocessable_entity }
      end
    end
  end

private
  def projekt_params
    params.require(:projekt).permit(:nazev, :kod, :active)
  end

end
